FROM python:3.11-slim

RUN addgroup --system --gid 1000 olumulo \
  && adduser --system --no-create-home --uid 1000 olumulo \
  && mkdir -p /usr/share/vosk \
  && chown -R olumulo /usr/share/vosk

COPY build/requirements.txt /opt/oremi/

RUN pip install --no-cache-dir -r /opt/oremi/requirements.txt

# Binaries
COPY bin/* /opt/oremi/bin/
RUN chmod +x /opt/oremi/bin/*

# Copy application files
COPY pyproject.toml LICENSE /opt/oremi/
COPY scripts/ /opt/oremi/scripts
COPY andika /opt/oremi/andika

ARG PACKAGE_NAME
ARG PACKAGE_VERSION

USER olumulo

ENV PATH="/opt/oremi/bin:/opt/oremi/scripts:$PATH"
ENV PYTHONPATH="/opt/oremi:$PYTHONPATH"

ENV TZ="Africa/Douala"
ENV MODELS_DIR="/usr/share/vosk"

EXPOSE 5315
VOLUME /usr/share/vosk

ENTRYPOINT ["/opt/oremi/bin/entrypoint.sh"]
