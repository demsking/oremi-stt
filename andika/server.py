# Copyright 2023-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import asyncio
import concurrent.futures
import json
import logging
import os
import traceback

import websockets.exceptions
import websockets.legacy.server
from oremi_core.wsserver import WebSocketConnection
from oremi_core.wsserver import WebSocketServer
from vosk import KaldiRecognizer
from vosk import Model
from vosk import SpkModel

from .models import InitMessage
from .models import RecognizerEntry
from .models import RecognizerSetting
from .package import APP_NAME
from .package import APP_VERSION

__all__ = [
  'Server',
]


SERVER_NAME = f'{APP_NAME}/{APP_VERSION}'
MAX_REASON_LENGTH = 123


class ModelNotFound(Exception):
  pass


class Server(WebSocketServer):
  def __init__(
    self,
    *,
    models_dir: str,
    enable_gpu: bool,
    cert_file: str | None = None,
    key_file: str | None = None,
    password: str | None = None,
    logger: logging.Logger,
  ) -> None:
    super().__init__(
      server_header=SERVER_NAME,
      cert_file=cert_file,
      key_file=key_file,
      password=password,
      logger=logger,
    )
    self.verbose = logger.level != logging.INFO
    self.recognizer_settings: dict[str, RecognizerSetting] = {}
    self.recognizer_entries: dict[str, RecognizerEntry] = {}
    self.pool = self._create_pool(enable_gpu)
    self._loop = asyncio.get_running_loop()
    self._load_models_dir(models_dir)

  @property
  def existing_models(self) -> list[str]:
    return list(self.recognizer_settings.keys())

  @staticmethod
  def truncate_reason(reason: str) -> str:
    if len(reason) > MAX_REASON_LENGTH:
      return reason[: MAX_REASON_LENGTH - 3] + '...'
    return reason

  @staticmethod
  def process_chunk(recognizer: KaldiRecognizer, chunk: bytes | str):
    if isinstance(chunk, str) and 'eof' in chunk:
      return recognizer.FinalResult()
    if recognizer.AcceptWaveform(chunk):
      return recognizer.Result()
    return recognizer.PartialResult()

  def _create_pool(self, enable_gpu: bool):
    if enable_gpu:
      from vosk import GpuInit, GpuThreadInit

      GpuInit()

      def thread_init() -> None:
        GpuThreadInit()

      return concurrent.futures.ThreadPoolExecutor(initializer=thread_init)
    return concurrent.futures.ThreadPoolExecutor(os.cpu_count() or 1, thread_name_prefix=APP_NAME)

  def _load_models_dir(self, models_dir: str):
    self.logger.info(f'Registering models settings from {models_dir}')
    if os.path.exists(models_dir):
      for model_name in os.listdir(models_dir):
        model_path = os.path.join(models_dir, model_name)
        if os.path.isdir(model_path):
          self.logger.info(f'Register model {model_name}')
          self.recognizer_settings[model_name] = RecognizerSetting(model_path=model_path)

  def _get_local_recognizer_entry(self, model_name: str, setting: RecognizerSetting) -> RecognizerEntry:
    if model_name not in self.recognizer_entries:
      self.logger.info(f'Loading model {model_name}')
      model = Model(setting.model_path)
      spk_model = SpkModel(setting.model_path_spk_path) if setting.model_path_spk_path else None
      entry = RecognizerEntry(model=model, spk_model=spk_model)
      self.recognizer_entries[model_name] = entry
    return self.recognizer_entries[model_name]

  async def _process_request(self, websocket: WebSocketConnection, message: bytes):
    return super()._process_request(websocket, message)

  async def _handle_audio_data(self, websocket: websockets.legacy.server.WebSocketServerProtocol) -> None:
    loop = asyncio.get_running_loop()
    init_timeout_timer_handler = loop.call_later(
      5,
      lambda: loop.create_task(
        websocket.close(code=1002, reason='Init Timeout'),
        name='Init Timeout Task',
      ),
    )

    try:
      message = await websocket.recv()
      init_timeout_timer_handler.cancel()
      request = InitMessage.from_json(message)  # type: ignore

      if request.model not in self.recognizer_settings:
        raise ModelNotFound(f'Model {request.model} not found')

      setting = self.recognizer_settings[request.model]
      recognizer_entry = self._get_local_recognizer_entry(request.model, setting)
    except (ValueError, AttributeError, TypeError, ModelNotFound) as error:
      error_message = f'Invalid Init Message: {message}. Error: {error}'
      await websocket.close(code=1003, reason=Server.truncate_reason(error_message))
      self.logger.error(error_message)

      if self.verbose:
        traceback.print_exc()

      return

    # Create the recognizer, word list is temporary disabled since not every model supports it
    if request.phrase_list and len(request.phrase_list) > 0:
      recognizer = KaldiRecognizer(
        recognizer_entry.model, request.samplerate, json.dumps(request.phrase_list, ensure_ascii=False)
      )
    else:
      recognizer = KaldiRecognizer(recognizer_entry.model, request.samplerate)

    recognizer.SetWords(request.show_words)
    recognizer.SetMaxAlternatives(request.max_alternatives)

    if recognizer_entry.spk_model:
      recognizer.SetSpkModel(recognizer_entry.spk_model)

    self.logger.info(f'Connection from {websocket.remote_address} {websocket.request_headers["User-Agent"]}')
    await self._send_ready_message(websocket)

    try:
      async for chunk in websocket:
        response = await self._loop.run_in_executor(self.pool, Server.process_chunk, recognizer, chunk)
        await websocket.send(response)
    except websockets.exceptions.ConnectionClosedOK as exception:
      await self._handle_connection_close(websocket, exception)
    except Exception as exception:
      error_message = f'Unexpected Transcribing Error: {exception}'
      await websocket.close(code=1003, reason=Server.truncate_reason(error_message))
      self.logger.error(error_message)

      if self.verbose:
        traceback.print_exc()
    finally:
      del recognizer

  async def _handle_messages(self, websocket: WebSocketConnection) -> None:
    try:
      await self._send_init_message(websocket)
      await self._handle_audio_data(websocket)
    except Exception as exception:
      error_message = f'Unexpected error: {exception}'
      self.logger.error(error_message)
      await websocket.close(code=1003, reason=Server.truncate_reason(error_message))

  async def _send_init_message(self, websocket: WebSocketConnection) -> None:
    event = {
      'type': 'init',
      'server': SERVER_NAME,
      'models': self.existing_models,
    }

    await websocket.send(json.dumps(event))

  async def _send_ready_message(self, websocket: WebSocketConnection) -> None:
    event = {
      'type': 'ready',
    }

    await websocket.send(json.dumps(event))
