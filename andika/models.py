# Copyright 2023 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from dataclasses import dataclass
from typing import Literal

from dataclasses_json import dataclass_json
from vosk import Model
from vosk import SpkModel


@dataclass_json
@dataclass
class InitMessage:
  type: Literal['init']
  samplerate: int
  model: str
  show_words: bool = True
  max_alternatives: int = 0
  phrase_list: list[str] | None = None


@dataclass
class RecognizerSetting:
  model_path: str
  model_path_spk_path: str | None = None


@dataclass
class RecognizerEntry:
  model: Model
  spk_model: SpkModel | None
