APP_NAME := $(shell python metadata.py name)
APP_VERSION := $(shell python metadata.py version)
IMAGE_NAME := demsking/$(APP_NAME)

SSL_PATH := ~/.config/oremi/ssl
SSL_CERT_FILE := $(SSL_PATH)/localhost.pem

.PHONY: all certificates clean dist publish-package image prepare publish-image test models

# start dev env
shell:
	tmuxinator

clean:
	rm -rf dist/

models:
	./scripts/vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-en-us-0.22-lgraph.zip' "en-us-small"
	./scripts/vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-small-fr-0.22.zip' "fr-small"
	./scripts/vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-fr-0.22.zip' "fr-big"
	./scripts/vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-fr-0.6-linto-2.2.0.zip' "fr-linto"

install: models
	poetry install

help:
	python -m andika -h

client:
	python client.py \
	  --port 5315

lint:
	pre-commit run --all-files

fix:
	ruff check . --fix

test:
	pytest

coverage:
	pytest --cov=oremi

coverage-html:
	pytest --cov=oremi --cov-report=html

outdated:
	poetry show --outdated

update:
	poetry update
	nix flake update
	pre-commit autoupdate

# Build a distribution of the project using poetry and twine
dist:
	rm -rf dist/
	poetry build --no-cache --format=wheel
	twine check dist/*

pypi: dist
	twine upload -r testpypi dist/*

build/requirements.txt: pyproject.toml poetry.lock
	mkdir -p build/
	poetry export --only=main --without-hashes -f requirements.txt -o $@

build/context:
	docker buildx create --name $(APP_NAME) --bootstrap
	mkdir -p build/
	touch $@

image: build/requirements.txt build/context
	docker buildx use $(APP_NAME)
	docker buildx build . \
	  --platform linux/amd64,linux/arm64 \
	  --push \
	  --progress plain \
	  --tag $(IMAGE_NAME):$(APP_VERSION) \
	  --tag $(IMAGE_NAME):latest \
	  --build-arg PACKAGE_NAME="$(APP_NAME)" \
	  --build-arg PACKAGE_VERSION="$(APP_VERSION)" \
	  --label "org.opencontainers.image.title=$(APP_NAME)" \
	  --label "org.opencontainers.image.description=$(shell python metadata.py description)" \
	  --label "org.opencontainers.image.version=$(APP_VERSION)" \
	  --label "org.opencontainers.image.revision=$(shell git rev-parse HEAD)" \
	  --label "org.opencontainers.image.authors=$(shell python metadata.py authors)" \
	  --label "org.opencontainers.image.created=$(shell date --rfc-3339=seconds)" \
	  --label "org.opencontainers.image.source=$(shell python metadata.py repository)" \
	  --label "org.opencontainers.image.url=$(shell python metadata.py repository)" \
	  --label "org.opencontainers.image.documentation=$(shell python metadata.py documentation)" \
	  --label "org.opencontainers.image.vendor=Oremi" \
	  --label "org.opencontainers.image.licenses=$(shell python metadata.py license)"

publish: image
	git commit pyproject.toml -m "Release $(APP_VERSION)"
	git tag v$(APP_VERSION)
	git push --tags origin main
