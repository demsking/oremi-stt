#!/bin/bash

# Copyright 2023 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

set +e

APP_NAME="oremi-andika"
MODELS_DIR="$1"  # Models directory passed as argument
TEMP_DIR="/tmp/vosk-models"

log_info() {
  echo -e "\e[32m$(date +'%Y-%m-%d %H:%M:%S') \e[37m- \e[34m$APP_NAME \e[37m- \e[90mINFO \e[37m- $1\e[0m"
}

log_error() {
  echo -e "\e[32m$(date +'%Y-%m-%d %H:%M:%S') \e[37m- \e[34m$APP_NAME \e[37m- \e[31mERROR \e[37m- $1\e[0m" >&2
  exit 1
}

cleanup() {
  log_info "Cleaning up temporary files..."
  rm -rf $TEMP_DIR
}

install() {
  if [ ! -d "$MODELS_DIR/$2" ]; then
    log_info "Downloading and installing $2..."
    mkdir -p "$(dirname $2)"
    mkdir -p $TEMP_DIR
    if curl -skL "$1" -o "$TEMP_DIR/$2.zip" && unzip -q "$TEMP_DIR/$2.zip" -d $TEMP_DIR && mkdir -p "$MODELS_DIR/$2" && mv $TEMP_DIR/vosk-model-*/* "$MODELS_DIR/$2"; then
      cleanup
      log_info "Model $2 installed successfully."
    else
      log_error "Failed to download and install $2."
    fi
  else
    log_info "Model $2 already installed. Skipping..."
  fi
}

# Check if MODELS_DIR argument is provided
if [ -z "$MODELS_DIR" ]; then
  log_error "MODELS_DIR argument not provided."
fi

# Call the install function with provided arguments
install "$2" "$3"
