# Oremi Andika

[![Buy me a beer](https://img.shields.io/badge/Buy%20me-a%20beer-1f425f.svg)](https://www.buymeacoffee.com/demsking)

Oremi Andika serves as the foundational pillar of the Oremi project, introducing
cutting-edge STT (Speech-to-Text) capabilities to enable seamless transcription
of spoken language.

Built upon the robust [Vosk Speech Recognition framework](https://alphacephei.com/vosk/),
this module empowers the Oremi personal assistant to accurately transcribe
spoken words into text, facilitating effortless communication and interaction.

Drawing inspiration from the Swahili word for *'transcribe'*, Andika
encapsulates the essence of transforming spoken language into written text.
By embodying this linguistic concept, Oremi Andika not only facilitates
efficient communication but also bridges the gap between spoken and written
expression, empowering users to seamlessly interact with their personal
assistant through natural speech while enjoying the convenience of textual
representation.

## Table of Contents

- [Installing Vosk Speech Recognition Models](#installing-vosk-speech-recognition-models)
- [Getting Started with Oremi Andika](#getting-started-with-oremi-andika)
- [Starting the Server with GPU](#starting-the-server-with-gpu)
- [Environment Variables](#environment-variables)
- [Starting the Server with Certificates](#starting-the-server-with-certificates)
- [Oremi Speech-to-Text Protocol](#oremi-speech-to-text-protocol)
  * [Initialization](#initialization)
  * [Speech Recognition](#speech-recognition)
  * [Connection Closure Codes](#connection-closure-codes)
  * [Example Implementation](#example-implementation)
- [Contribute](#contribute)
- [Versioning](#versioning)
- [License](#license)

## Installing Vosk Speech Recognition Models

To use Oremi Andika you'll need to install the required Vosk models.

**Prerequisites**

- Make sure you have `Bash`, `curl` and `unzip` installed on your system.

**Installation Steps**

1. Download the install script from the Oremi Andika repository:
   [vosk-models-install.sh](https://gitlab.com/demsking/oremi-andika/-/raw/main/scripts/vosk-models-install.sh?inline=false)

2. Make the script executable:

   ```bash
   chmod +x vosk-models-install.sh
   ```

3. Obtain the model URL and name you want to install. You can find a list of
   available models at [Vosk Models](https://alphacephei.com/vosk/models).

4. Run the script with the following command:

   ```sh
   ./vosk-models-install.sh /path/to/models_directory "model_url" "model_name"
   ```

   Replace `/path/to/models_directory` with the directory where you want to
   install the models. `"model_url"` should be the URL of the model you want to
   install, and `"model_name"` should be the desired name for the model.

   **Examples**

   ```sh
   # install the English model named "en-us-small" in the directory "~/.cache/vosk/models"
   ./vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-en-us-0.22-lgraph.zip' "en-us-small"

   # install the French model named "fr-small" in the directory "~/.cache/vosk/models"
   ./vosk-models-install.sh ~/.cache/vosk/models 'https://alphacephei.com/vosk/models/vosk-model-small-fr-0.22.zip' "fr-small"
   ```

   The installation script will download and install the model. Temporary files
   will be cleaned up automatically after installation.

## Getting Started with Oremi Andika

The easiest way to use Oremi Andika is with Docker. Start with Docker for a quick
setup. Follow these steps:

**Using Docker**

Use the official Docker image [`demsking/oremi-andika`](https://hub.docker.com/r/demsking/oremi-andika)
to run Oremi Andika:

```sh
docker run -d \
  --env-file <path_to_env_file> \
  -p 5315:5315 \
  -v ~/.cache/vosk/models:/usr/share/vosk \
  demsking/oremi-andika
```

Replace `<path_to_env_file>` with the path to your environment variable file
containing the necessary configurations.

**Alternative Installation**

If you prefer installing Oremi Andika directly:

```sh
pip install oremi-andika
```

After installation, start Oremi Andika using the provided command.

```sh
usage: oremi-andika [-h] [-m MODELS] [--enable-gpu] [--host HOST] [-p PORT] [--cert-file CERT_FILE] [--key-file KEY_FILE] [--password PASSWORD] [-v]

Empowering seamless transcription with cutting-edge STT (Speech-to-Text) technology, revolutionizing interaction through accurate speech recognition

options:
  -h, --help            show this help message and exit
  -m MODELS, --models MODELS
                        Path to the models directory (default: ~/.cache/vosk/models).
  --enable-gpu          Enable GPU usage.
  --host HOST           Host address to listen on (default: 127.0.0.1).
  -p PORT, --port PORT  Port number to listen on (default: 5315).
  --cert-file CERT_FILE
                        Path to the certificate file for secure connection.
  --key-file KEY_FILE   Path to the private key file for secure connection.
  --password PASSWORD   Password to unlock the private key (if protected by a password).
  -v, --version         Show the version of the application.
```

## Starting the Server with GPU

If you have a compatible GPU and want to leverage its power, you can start the
Oremi Andika server with GPU support:

```sh
docker run -d \
    --runtime=nvidia \
    -p 5315:5315 \
    -v ~/.cache/vosk:/usr/share/vosk \
  demsking/oremi-andika --enable-gpu
```

## Environment Variables

The following environment variables can be used to configure the Oremi Andika
Docker containers:

| Variable        | Description                     | Default Value |
|-----------------|---------------------------------|---------------|
| LOG_LEVEL       | Logging level                   | "info"        |
| LOG_FILE        | Log file path                   |               |

## Starting the Server with Certificates

To start the Oremi Andika server with a certificate, you can use the
`--cert-file` and `--key-file` options to specify the certificate and private
key files. Additionally, if your private key is password-protected, you can use
the `--password` option to provide the password. Here's how to proceed:

1. **Generate a Self-Signed SSL Certificate (For Testing):**

   If you're testing Oremi Andika locally, you can generate a self-signed
   SSL certificate.
   Follow these steps to generate a self-signed certificate using OpenSSL:

   ```sh
   # Generate a self-signed certificate
   openssl req -x509 -nodes -new -sha256 -days 365 -newkey rsa:2048 \
     -subj "/C=CM/CN=localhost" \
     -keyout key.pem \
     -out cert.pem
   ```

   It's important to note that these certificates are self-signed, which means
   they are not issued by a recognized Certificate Authority (CA). While
   suitable for testing and development purposes, self-signed certificates may
   trigger security warnings in browsers and other client applications.

   Please note that this is a simplified example for generating self-signed
   certificates for testing purposes. In production environments, it's
   recommended to obtain SSL certificates from a trusted Certificate
   Authority (CA) like [Let's Encrypt](https://letsencrypt.org/) to ensure
   security and proper authentication..
   Let's Encrypt provides free and automated certificates that are recognized
   by most browsers and clients.

2. **Start the Server using Docker:**

   The quickest way to start the Oremi Andika server with certificates is by
   using Docker. Run the following command in your terminal:

   ```sh
   docker run -d \
      -p 5315:5315 \
      -v ~/.cache/vosk/models:/usr/share/vosk \
      -v /path/to/cert.pem:/cert.pem \
      -v /path/to/key.pem:/key.pem \
    demsking/oremi-andika \
      --cert-file /cert.pem \
      --key-file /key.pem \
      --password your_private_key_password
   ```

   > Replace `/path/to/cert.pem`, `/path/to/key.pem`, and
   > `your_private_key_password` with the appropriate values.

   This command mounts the certificate and key files into the Docker container
   and starts the server.

## Oremi Speech-to-Text Protocol

Oremi Andika operates using a WebSocket-based protocol for real-time speech
recognition. The process involves initialization, audio streaming, and result
transmission. Here's how Oremi Andika works:

### Initialization

**1. Client**

When a client establishes a connection, it first waits for the server's
initialization message:

```py
server_init_message = await websocket.recv()
print(server_init_message)
```

This message contains the server's version and a list of supported models:

```json
{
  "type": "init",
  "server": "oremi-andika/2.0.0b9",
  "models": ["fr-big", "en-us-small", "fr-small", "fr-linto"]
}
```

Once received, the client sends an initial JSON initiation message to the
server:

```json
{
  "type": "init",
  "model": "fr-small",
  "samplerate": 16000.0
}
```

The client then waits for the server's readiness message.

**2. Server**

The server then sends ready message:

```json
{
  "type": "ready"
}
```

**Note:** If the client doesn't send the initialization message within **5
seconds**, the server will close the connection with code `1002` and reason
`Init Timeout`.

### Speech Recognition

**1. Client**

Once the session is initialized, the client streams audio data in bytes to the
server, with an audio frequency of **16000Hz** and a **single channel**.

**2. Server**

The server processes the audio stream and sends partial results to the client
as JSON messages:

```json
{
  "partial": "bonjour"
}
```

Upon detecting a pause in speech, the server sends the final result along with
confidence scores:

```json
{
  "result" : [
    {
      "conf" : 0.954096,
      "end" : 1.529221,
      "start" : 1.050000,
      "word" : "bonjour"
    }, {
      "conf" : 1.000000,
      "end" : 1.680000,
      "start" : 1.530743,
      "word" : "le"
    }, {
      "conf" : 1.000000,
      "end" : 2.100000,
      "start" : 1.680000,
      "word" : "monde"
    }
  ],
  "text" : "bonjour le monde"
}
```

### Connection Closure Codes

Possible connection closure codes:

- `1000`: Indicates a normal closure, meaning that the purpose for which the
  connection was established has been fulfilled.
- `1002`: Init Timeout
- `1003`: Invalid Message
- `4000`: Unexpected Error

### Example Implementation

For an example of how to implement a client for the "Oremi Andika" you can refer
to the [client.py file](https://gitlab.com/demsking/oremi-andika/blob/main/client.py)
in the GitLab repository.
The example demonstrates how to connect to the server, send audio data, and
handle the JSON messages received from the server.

## Contribute

Please follow [CONTRIBUTING.md](https://gitlab.com/demsking/oremi-andika/blob/main/CONTRIBUTING.md).

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner,
  and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions
to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.
You may obtain a copy of the License at [LICENSE](https://gitlab.com/demsking/oremi-andika/blob/main/LICENSE).
